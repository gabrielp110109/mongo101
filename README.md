ACTIVIDAD MONGO 101

Se hace pull de la imagen de MongoDB utilizando Docker

Repositorio
git clone git@gitlab.com:gabrielp110109/mongo101.git


Uso
Primero usaremos un contenedor de docker con mongo, para eso ejecutamos el siguiente comando:

docker run --name mongodb -d -p 27017:27017 mongodb/mongodb-community-server

docker exec -ti --name mongoimport -d students -c grades --file /path/to/grades.json
Ahora para entrar al cliente de mongo ejecute lo siguiente:

docker exec -ti mongodb mongosh

Gabriel Isai Prieto SAenz
353297
Universidad Autonoma de Chihuahua
Web Platforms